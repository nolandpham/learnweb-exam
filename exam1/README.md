Thực hành làm Web App
===================

#### Yêu cầu:
 - Làm giao diện response cho mobile và tablet( ex: iphone7, iPad)
 - Dùng HTML, CSS(Bootstrap), Javascript(jQuery) để tạo giao diện.
 - Dùng jQuery gọi lên Flickr search API để lấy data.
 - Thực hiện chức năng paging( 5 item per page), khi kéo đến cuối trang thì load page tiếp.
 - Upload source code lên https://bitbucket.org/nolandpham/learnweb-exam/exam1/{user_name}

#### Deadline: hết thứ 2, 18-12-2017
